
default: build

build:
	rm -r vendor
	rm go.sum
	qtdeploy -docker build

build-linux:
	rm -r vendor
	rm go.sum
	qtdeploy -docker build linux_static

build-debug:
	rm -r vendor
	rm go.sum
	qtdeploy -docker -quickcompiler build


build-windows-64:
	rm -r vendor
	rm go.sum
	qtdeploy -docker build windows_64_static

build-wasm:
	rm -r vendor
	rm go.sum
	qtdeploy -docker build wasm


clean:
	rm -r deploy
	rm -r vendor
	rm go.sum

