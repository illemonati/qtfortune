package main

import (
	"fmt"
	"github.com/therecipe/qt/widgets"
	"math/rand"
	"os"
)




func main() {
	app := widgets.NewQApplication(len(os.Args), os.Args)

	window := widgets.NewQMainWindow(nil, 0)
	fortuneButton := widgets.NewQPushButton2("Click here to get your fortune !", nil)
	fortuneButton.ConnectClicked(func(bool) {
		fortuneButton.SetText(fmt.Sprintf("Your lucky numbers are %d %d %d\nHave a nice day!", rand.Intn(27),  rand.Intn(27),  rand.Intn(27)))
	})
	window.SetCentralWidget(fortuneButton)
	window.Show()
	app.Exec()
}
